
App = {
  loading: false,
  contracts: {},

  load: async () => { //Cargar pagina en el navegador
    await App.loadWeb3()
    await App.loadAccount()
    await App.loadContract()
    await App.render()
  },

  
  loadWeb3: async () => { //Inyectar web3 en navegador (Metamask WAY)
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider
      web3 = new Web3(web3.currentProvider)
    } else {
      //window.alert("Please connect to Metamask.") DESCOMENTAR
    }
    // Browsers compatibles directamente con Ethereum...
    if (window.ethereum) {
      window.web3 = new Web3(ethereum)
      try {
        // Peticion para cargar cuenta
        await ethereum.enable()
        web3.eth.sendTransaction({/* ... */})
      } catch (error) {
        // Si no se permite el acceso...
      }
    }
    // Navegadores que pueden permitir conexión a web3...
    else if (window.web3) {
      App.web3Provider = web3.currentProvider
      window.web3 = new Web3(web3.currentProvider)
      // Acccounts always exposed
      web3.eth.sendTransaction({/* ... */})
    }
    // Si no permite web3...
    else {
      console.log('Non-Ethereum browser detected. You should consider trying MetaMask!')
    }
  },

  loadAccount: async () => {
    // Cargamos la cuenta a utilizar
    App.account = web3.eth.accounts[0]
    web3.eth.defaultAccount=web3.eth.accounts[0]  //Mostrar clave publica en APP
    
    console.log("account is : " + App.account)
  },

  loadContract: async () => {
    // Create a JavaScript version of the smart contract
    /**Cargamos el .json del contrato(Creado al compilar nuestro contrato) */
    const todoList = await $.getJSON('TodoList.json') 
    App.contracts.TodoList = TruffleContract(todoList)
    App.contracts.TodoList.setProvider(App.web3Provider)

    // Permitimos al Smart Contract leer datos de la blockchain
    App.todoList = await App.contracts.TodoList.deployed()
  },

  render: async () => {
    // Evitar que la pagina "cargue" si ya lo está haciendo
    if (App.loading) {
      return
    }

    // La ponemos a cargar si no lo está --> setLoading()
    App.setLoading(true)

    // Cargamos la cuenta (clave publica) en el html
    $('#account').html(App.account)

    // Cargamos las tareas
    await App.renderTasks()

    // Quitamos pantalla de carga si estan las tareas --> setLoading()
    App.setLoading(false)
  },

  renderTasks: async () => {
    /**Importamos taskCount de nuestro contrato*/
    const taskCount = await App.todoList.taskCount()
    const $taskTemplate = $('.taskTemplate')

    //Cargamos cada tarea crando una vista para cada unaaa
    for (var i = 1; i <= taskCount; i++) {
      // Cargamos en cada tarea que exista su correspondiente informacion
      const task = await App.todoList.tasks(i) //esta constante es un array
      const taskId = task[0].toNumber()
      const taskContent = task[1]
      const taskCompleted = task[2]

      // Creamos el HTML para mostrar la tareaa
      const $newTaskTemplate = $taskTemplate.clone()
      $newTaskTemplate.find('.content').html(taskContent)
      $newTaskTemplate.find('input')
                      .prop('name', taskId)
                      .prop('checked', taskCompleted)
                      .on('click', App.toggleCompleted)

      /**Dependiendo de si esta acabada o no, estará en una lista u otra */
      if (taskCompleted) {
        $('#completedTaskList').append($newTaskTemplate)
      } else {
        $('#taskList').append($newTaskTemplate)
      }

      /**Mostramos la tarea */
      $newTaskTemplate.show()
    }
  },

  createTask: async () => {
    App.setLoading(true); //carga el loading en pagina
    setInterval(reload,15000);
    const content = $('#newTask').val()
    await App.todoList.createTask(content);

    function reload(){
      window.location.reload()
    }
    
  },

  toggleCompleted: async (e) => { //onclick event e
    App.setLoading(true); //carga el loading en pagina
    setInterval(reload,15000);
    const taskId = e.target.name //e = evento sobre el target ID 
    await App.todoList.toggleCompleted(taskId);
    
    function reload(){
      window.location.reload()
    }
    
  },

  setLoading: (boolean) => { /**Mostramos y/o ocultamos Pantalla de carga / Contenido (Tareas) */
    App.loading = boolean //Para el render
    const loader = $('#loader')
    const content = $('#content')
    if (boolean) {
      loader.show()
      content.hide()
    } else {
      loader.hide()
      content.show()
    }
  }
}

$(() => {
  $(window).load(() => {
    App.load()
  })
})
