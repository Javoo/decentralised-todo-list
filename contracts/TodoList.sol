pragma solidity ^0.5.0;

contract TodoList { //creamos el contrato
  /**Variable que trackea el numero de tareas existentes */
  uint public taskCount = 0; //uint = integer que no puede ser negativo


  /**Para modelar las tareas lo haremos mediante un struct */
  struct Task {
    uint id;
    string content;
    bool completed;
  }


  /**Diccionario, guardamos en "tasks" un par clave-valor (uint - Task) en este caso */
  /**Hay que referenciar los items uno a uno, tampoco podemos mostrar toda la lista de golpe */
   //Con public, Solidity nos otorga una funcion Tasks para referenciar el mapeo
  mapping(uint => Task) public tasks;

/**Los eventos notifican a las aplicaciones sobre el cambio 
  realizado en los contratos y aplicaciones que se pueden 
  usar para ejecutar la lógica dependiente. */
  event TaskCreated(
    uint id,
    string content,
    bool completed
  );

  event TaskCompleted(
    uint id,
    bool completed
  );

  event editedTask(
    uint id,
    string content
  );

/*
  constructor() public { //función llamada justo al ejecutar el contrato
    createTask("Primera tarea de prueba"); //default task
  }
*/

/**Crear tarea */
  function createTask(string memory _content) public {
    taskCount ++;
    tasks[taskCount] = Task(taskCount, _content, false);
    emit TaskCreated(taskCount, _content, false); //Llamar la funcion externamente
  }

/**Marcar tarea, completar tarea */
  function toggleCompleted(uint _id) public {
    Task memory _task = tasks[_id];
    _task.completed = !_task.completed;
    tasks[_id] = _task;
    emit TaskCompleted(_id, _task.completed);
  }


  /*function editTask(uint _id, string memory _content) public{
    Task memory _task = tasks[_id];
    tasks[_id].content = _content;
    emit editedTask(_id, _content);
  }*/
}